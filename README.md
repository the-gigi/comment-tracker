# Comment Tracker

Keep track of comments in my [Tuts+ articles](https://tutsplus.com/authors/gigi-sayfan)

This code accompanies this Tuts+ article - [Modern Web Scraping With BeautifulSoup and Selenium](http://code.tutsplus.com/tutorials/modern-web-scraping-with-beautifulsoup-and-selenium--cms-30486) (Inception much?)

# Installation

Make sure you got [Python 3](https://www.python.org/downloads/) and [pipenv](https://docs.pipenv.org/) and then:

`pipenv install`
