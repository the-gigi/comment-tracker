import time
from datetime import timedelta

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

from comment_counter import get_comment_count

HOUR = timedelta(hours=1)


def get_page(url):
    r = requests.get(url)
    content = r.content.decode('utf-8')
    return BeautifulSoup(content, 'html.parser')


def get_page_articles(page):
    articles = [a.a.attrs['href'] for a in page.findAll('article')]
    return articles


def get_all_articles(author):
    base_url = f'https://tutsplus.com/authors/{author}?page='
    url = base_url + '1'
    page = get_page(url)
    articles = get_page_articles(page)

    pagination_buttons = page.find_all('a', {'class': 'pagination__button'})
    page_urls = [base_url + p.text for p in pagination_buttons[:-1]]
    for url in page_urls:
        page = get_page(url)
        articles += get_page_articles(page)

    return articles


def track_comments():
    """
    :return:
    """
    articles = get_all_articles('gigi-sayfan')
    driver = webdriver.Chrome()
    try:
        total_comments = 0
        articles_with_comments = 0
        for article in articles:
            name, comment_count, last_comment = get_comment_count(driver, article)
            if comment_count > 0:
                articles_with_comments += 1
                total_comments += comment_count
                print(f'Article Name: {name}')
                print(f'Comments: {comment_count}')
                if last_comment:
                    print (f"Last comment: {last_comment['title']} {last_comment['when']}")
                print()
        print(f'{articles_with_comments} articles have a total of {total_comments} comments!')
        print()
    except Exception as e:
        print(e)
    finally:
        driver.close()
        time.sleep(HOUR.seconds)


def main():
    while True:
        track_comments()


if __name__ == '__main__':
    main()
