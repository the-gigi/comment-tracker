from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait


def get_comment_count(driver, url):
    """
    """
    driver.get(url)

    name = driver.find_element_by_class_name('content-banner__title').text
    disqus_iframe = driver.find_element_by_id('disqus_thread').find_element_by_tag_name('iframe')
    iframe_url = disqus_iframe.get_attribute('src')

    driver.get(iframe_url)
    wait = WebDriverWait(driver, 5)
    commentCountPresent = presence_of_element_located((By.CLASS_NAME, 'comment-count'))
    wait.until(commentCountPresent)

    comment_count_span = driver.find_element_by_class_name('comment-count')
    comment_count = int(comment_count_span.text.split()[0])

    last_comment = {}
    if comment_count > 0:
        last_author = driver.find_elements_by_class_name('author')[-1].find_element_by_tag_name('a')
        last_author = last_author.get_attribute('data-username')
        if last_author != 'the_gigi':
            last_comment_meta = driver.find_elements_by_class_name('post-meta')[-1].find_element_by_tag_name('a')
            last_comment = dict(author=last_author,
                                title=last_comment_meta.get_attribute('title'),
                                when=last_comment_meta.text)
    return name, comment_count, last_comment


def main(driver):
    articles = (
        'testing-data-intensive-code-with-go-part-5--cms-29852',
        'mastering-the-react-lifecycle-methods--cms-29849'
    )

    for article in articles:
        url = 'https://code.tutsplus.com/tutorials/' + article
        name, comment_count, last_comment = get_comment_count(driver, url)
        last_comment = f'{last_comment_date} {when}' if comment_count > 0 else ''
        print(f'Article Name: {name}\nComments: {comment_count} {last_comment}\n')


if __name__ == '__main__':
    driver = webdriver.Chrome()
    try:
        main(driver)
    finally:
        driver.close()
